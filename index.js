"use strict";

let Discord = require('discord.io');

let logger = require('winston');
let auth = require('./auth.json');

const cheerio = require('cheerio');
const request = require('request');

let moment = require('moment-timezone');

// Unsplash (penguin) dependencies
const Unsplash = require('unsplash-js').default;
global.fetch = require('node-fetch');

const unsplash = new Unsplash({
    applicationId: "25185b70a323a692fbe9b6ffb24537100fc383e506eb85ab1d98852342e8987a",
    secret: "8c17f4af083e55d138d5815489427d65d02a9309e57293e31f7b404515f9cc21",
    callbackUrl: "https://evolution36.com"
});

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';


// Initialize Discord Bot
let bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

let prefix = '/';
let puzzles = ['sailing', 'rigging', 'carpentry', 'patching', 'bilging', 'gunning', 'treasure-haul', 'navigating', 'battle-navigation', 'swordfighting', 'rumble', 'drinking', 'spades', 'hearts', 'treasure-drop', 'poker', 'distilling', 'alchemistry', 'shipwrightery', 'blacksmithing', 'foraging', 'weaving'];
let smh = {
    'kh': 'Kraken',
    'kraken': 'Kraken',
    'ci': 'Cursed Isles',
    'cursed isles': 'Cursed Isles',
    'cit': 'Atlantis',
    'atlantis': 'Atlantis',
    'hs': 'Haunted Seas',
    'haunted seas': 'Haunted Seas'
};

let commandsWithHelp = [
    {
        "name": "help",
        "value": "Prints the help message",
        "syntax": "help",
        "args": 0,
        "disabled": false
    },
    {
        "name": "ping",
        "value": "Pong. Used for testing the bot",
        "syntax": "ping",
        "args": 0,
        "disabled": false
    },
    {
        "name": "set-prefix",
        "value": "Set the command prefix",
        "syntax": "set-prefix /",
        "args": 1,
        "disabled": false
    },
    {
        "name": "penguin",
        "value": "Gives random image of a penguin. Now ain't that good aye?",
        "syntax": "penguin",
        "args": 0,
        "disabled": false
    },
    {
        "name": "who",
        "value": "Request a pirates information, ye can request it from other oceans too ye know",
        "syntax": "who [pirate] (ocean)",
        "args": 1,
        "disabled": false
    },
    {
        "name": "disable",
        "value": "Disable a certain command",
        "syntax": "disable [command]",
        "args": 1,
        "disabled": false
    },
    {
        "name": "enable",
        "value": "Enable a certain command. Ye'd better not disable this command pirate!",
        "syntax": "enable [command]",
        "args": 1,
        "disabled": false
    },
    {
        "name": "time",
        "value": "Request the current pirate time, use `/time confusing` for getting 12-hour notation or use `/time add [hh:mm] (confusing)` to do basic time conversion.",
        "syntax": "time (confusing) or /time add [hh:mm] (confusing)",
        "args": 0,
        "disabled": false
    },
    {
        "name": "mytime",
        "value": "Request a certain pirate time in your timezone. For example `/mytime 16:45 Europe/Amsterdam` will convert 16:45 pirate time to yer timezone of Europe/Amsterdam. When yer unsure, see https://en.wikipedia.org/wiki/List_of_tz_database_time_zones",
        "syntax": "mytime [hh:mm] [timezone]",
        "args": 2,
        "disabled": false
    },
    {
        "name": "standing",
        "value": "This command is not simple aye, see " + prefix + "standing help",
        "syntax": "standing [pirate] [puzzles] (ocean)",
        "args": 1,
        "disabled": false
    },
    {
        "name": "ego",
        "value": "For the insecure of pirates among us. Don't worry lad, not necessarily looking at ye",
        "syntax": "ego",
        "args": 0,
        "disabled": false
    },
    {
        "name": "status",
        "value": "Request server status and online pirate count, ye can request it for other oceans too ye know",
        "syntax": "status",
        "args": 0,
        "disabled": false,
    },
    {
        "name": "pvp",
        "value": "Request last PvP battle of a given pirate's crew, optionally provide an amount of battles if ye like (default is 1)",
        "syntax": "pvp [pirate]",
        "args": 0,
        "disabled": false,
    },
    {
        "name": "cstats",
        "value": "Request latest daily battle stats of a given pirate's crew, optionally provide a number of days ye like (default is 1)",
        "syntax": "cstats [pirate] (days)",
        "args": 1,
        "disabled": false
    },
    {
        "name": "createevent",
        "value": "Simplified event creation for DisCal bot. Be aware, this command uses comma's to separate parameters. Day&time should be of the format 16:45 or 2018/05/06-16:45:00. If only a time is specified, the first day (either today or tomorrow) the specified time takes place will be assumed as event day. Timezone is pirate time. Event duration is set to one hour.",
        "syntax": "createevent [name], [day&time] , (description)",
        "args": 2,
        "disabled": true
    },
    {
        "name": "smh",
        "value": "Request info about sea monster hunting map drops",
        "syntax": "smh (maptype)",
        "args": 0,
        "disabled": false
    },
    {
        "name": "pirate",
        "value": "Generates a random pirate name",
        "syntax": "pirate",
        "args": 0,
        "disabled": false
    }
];

let createEvent = [];
let createEventIndex = 0;
bot.on('message', function (user, userID, channelID, message, evt) {
    if (user.includes('DisCal') && createEvent !== undefined && createEvent.length > 0) {
        if (createEventIndex < createEvent.length) {
            setTimeout(function () {
                send(createEvent[createEventIndex]);
                createEventIndex++;
            }, 1000);
        } else if (createEventIndex === createEvent.length) {
            if (message.includes('Event successfully created!')) {
                bot.getMessages({
                    channelID: channelID,
                    before: evt.d.id,
                    limit: 2,
                }, function (error, messages) {
                    if (error) {
                        sendEmbed({
                            "title": "Barnacle, something went wrong mate!",
                            "description": error
                        });
                        return;
                    }
                    let deleteIDs = [evt.d.id];
                    for (message of messages) {
                        deleteIDs.push(message.id);
                    }
                    bot.deleteMessages({
                        channelID: channelID,
                        messageIDs: deleteIDs
                    }, function (error) {
                        if (error) {
                            sendEmbed({
                                "title": "Barnacle, something went wrong mate!",
                                "description": error
                            });
                        }
                        return;
                    });
                });
                sendEmbed({
                    "title": "Event created",
                    "description": "Yer event was successfully created in the calendar mate"
                });
            } else {
                send('&event cancel');
                sendEmbed({
                    "title": "Barnacle, something went wrong mate!",
                    "description": "Something went wrong with creating yer event, contact TheRien if ye don't know why."
                });
            }
            createEventIndex++;
        }
    }

    if (message.substring(0, 1) === prefix) {
        let args = message.substring(1).split(' ');
        let cmd = args.shift();

        if (!hasRightArgs(cmd, args)) {
            sendEmbed({
                "title": "Barnacle, something went wrong mate!",
                "description": "Either the command is disabled, ye gave the wrong amount of arguments or the command doesn't even exist! Maybe ye just had too much rum swabbie,  try `" + prefix + "help`."
            });
        } else {
            switch (cmd) {
                case 'help':
                    if (args.length > 0) {
                        let command = args.shift();
                        let commandObject = commandsWithHelp.find(function(c) { return c.name === command});
                        if (commandObject === undefined) {
                            sendEmbed({
                                "title": "Command not found",
                                "description": "The command who's help ye're lookin' for doesn't exist"
                            });
                        } else {
                            sendEmbed({
                                "title": "Help for " + command,
                                "description": commandObject.value + "\n" +
                                    "```" + prefix + commandObject.syntax + "```",
                                "footer": {
                                    "text": "[square brackets] means it's a parameter , (regular brackets) means it's optional. Got that, pirate?"
                                }
                            });
                        }
                    } else {
                        sendEmbed({
                            "title": "Help message",
                            "description": "The following commands are supported. Type `/help [command]` to get more help on a certain command.\n" +
                                commandsWithHelp.reduce(function(string, c) {
                                    return !c.disabled ? string + ", " + c.name : string;
                                },"").substring(2)
                        });
                    }
                    break;

                case 'ping':
                    send("Pong");
                    break;

                case 'set-prefix':
                    prefix = args.shift();
                    sendEmbed({
                        "title": "Changed prefix",
                        "description": "Prefix set to " + prefix
                    });
                    break;

                case 'penguin':
                    unsplash.photos.getRandomPhoto({query: "penguin"})
                        .then(response => {
                            response.json().then(json => {
                                sendEmbed({
                                    "title": "Penguin!",
                                    "image": {
                                        "url": json.urls.small
                                    }
                                });
                            });
                        });
                    break;

                case 'who':
                    if (args.length > 1) {
                        sendTitle(args[0], args[1]);
                    } else {
                        sendTitle(args[0]);
                    }
                    break;

                case 'disable':
                    for (let command of commandsWithHelp) {
                        if (command.name === args[0]) {
                            command.disabled = true;
                            sendEmbed({
                                "title": "Command disabled",
                                "description": args[0] + " is now disabled"
                            });
                            break;
                        }
                    }
                    sendEmbed({
                        "title": "Command not found",
                        "description": "The command ye tried to disable was not found. What are ye even doin' mate..."
                    });
                    break;

                case 'enable':
                    for (let command of commandsWithHelp) {
                        if (command.name === args[0]) {
                            command.disabled = false;
                            sendEmbed({
                                "title": "Command enabled",
                                "description": args[0] + " is now enabled",
                            });
                        }
                    }
                    sendEmbed({
                        "title": "Command not found",
                        "description": "The command ye tried to enable was not found. You alright pirate?"
                    });
                    break;

                case 'standing':
                    if (args[0] === 'help') {
                        sendEmbed({
                            "title": "Usage: `" + prefix + "standing [pirate] [puzzles] (ocean)`",
                            "description": "Ocean is an optional parameter and will default to obsidian. Puzzles can be one or more of the following:\n" +
                            puzzles.join(", ")
                        });
                    } else {
                        let ocean;
                        let pirate = args.shift();
                        if (puzzles.indexOf(args[args.length - 1]) === -1) {
                            ocean = args.pop();
                        }
                        // Pirate and ocean will be removed from args, so args is the remaining array of puzzles
                        sendStanding(pirate, args, ocean);
                    }
                    break;

                case 'time':
                    let format = 'HH:mm';
                    let confusing = false;
                    let lastArg = args.pop();
                    if (lastArg === 'confusing') {
                        confusing = true;
                        format = 'hh:mm A';
                    }
                    if (args.length === 0) {
                        sendEmbed({
                            "title": "Current pirate time (America/Los_Angeles)",
                            "description": moment().tz("America/Los_Angeles").format(format)
                        });
                    } else {
                        if (confusing) {
                            lastArg = args.pop();
                        }
                        let times = lastArg.split(':');
                        sendEmbed({
                            "title": "Calculated time",
                            "description": moment().tz("America/Los_Angeles").add({
                                hours: times[0],
                                minutes: times[1]
                            }).format(format)
                        });
                    }
                    break;

                case 'mytime':
                    let timeFormat = 'HH:mm';
                    let time = args.shift();
                    let timezone = args.shift();
                    let timeString = moment().tz("America/Los_Angeles").format("YYYY-MM-DD ") + time;
                    let yourTime = moment.tz(timeString, "America/Los_Angeles").clone().tz(timezone).format(timeFormat);
                    sendEmbed({
                        "title": "Yer time",
                        "description": "Converted " + time + " from pirate time to " + timezone + ": " + yourTime
                    });
                    break;

                case 'ego':
                    let egoMessages = [
                        "[pirate] be the best of all the seven seas!",
                        "I love ye [pirate]",
                        "No, sorry, not you [pirate], yer ego is good as it is",
                        "I'm yer biggest fan [pirate]!",
                        "If there be any pirate, be a better pirate than any other pirate be, it'd be this pirate: [pirate]"
                    ];
                    let message = egoMessages[Math.floor(Math.random() * egoMessages.length)].replace("[pirate]", user);
                    send(message);
                    bot.deleteMessage({
                        channelID: channelID,
                        messageID: evt.d.id
                    }, function (error) {
                        if (error) {
                            sendEmbed({
                                "title": "Barnacle, something went wrong mate!",
                                "description": error
                            });
                        }
                    });
                    break;

                case 'status':
                    let ocean = args.shift();
                    sendStatus(ocean);
                    break;

                case 'pvp':
                    if (!sendPvp(args.shift(), args.shift())){
                        sendEmbed({
                            "title": "Barnacle, something went wrong mate!",
                            "description": "Either the command is disabled, ye gave the wrong amount of arguments or the command doesn't even exist! Maybe ye just had too much rum swabbie,  try `" + prefix + "help`."
                        });
                    };
                    break;

                case 'cstats':
                    if (!sendStats(args.shift(), args.shift())){
                        sendEmbed({
                            "title": "Barnacle, something went wrong mate!",
                            "description": "Either the command is disabled, ye gave the wrong amount of arguments or the command doesn't even exist! Maybe ye just had too much rum swabbie,  try `" + prefix + "help`."
                        });   
                    };
                    break;

                case 'createevent':
                    args = args.join(' ').split(',').map(function (arg) {
                        return arg.trim();
                    });
                    let eventName = args.shift();
                    let eventTime = args.shift();
                    let eventDescription = args.join(',');
                    sendEvent(eventName, eventTime, eventDescription);
                    break;

                case 'smh':
                    if (args.length === 0) {
                        sendSMH();
                    } else {
                        sendSMH(args.join(" "));
                    }
                    break;

                case 'pirate':
                    let PirateNames1 = [
                        "Cap\'n",
                        "Plunderer",
                        "Captain",
                        "First-Mate",
                        "Seaman",
                        "\'ol",
                        "Tommy",
                        "Peg-Leg",
                        "Shark-Bait",
                        "Fingerless",
                        "Gold-tooth",
                        "Toothless",
                        "David",
                        "Davey",
                        "Steve",
                        "Barnacle",
                        "Matey"
                    ];
                    let PirateNames2 = [
                        "Pete",
                        "Blackbeard",
                        "Sea-Shanty",
                        "the Great",
                        "\'I ran out of ideas to put here\'",
                        "[pirate]",
                        "Frank",
                        "Gregory",
                        "Tom",
                        "the Deranged",
                        "the Deserter",
                        "the Shipwrecker",
                        "the Mighty",
                        "\'Just Steve\'",
                        "Two-Toes",
                        "\'Oh Ha Ha\' the Brave"
                    ];
                    let PirateMessage1 = PirateNames1[Math.floor(Math.random() * PirateNames1.length)].replace("[pirate]", user);
                    let PirateMessage2 = PirateNames2[Math.floor(Math.random() * PirateNames2.length)].replace("[pirate]", user);
                    send('Welcome \'yer new shipmate, ' + PirateMessage1 + ' ' + PirateMessage2 + '!');
                    bot.deleteMessage({
                        channelID: channelID,
                        messageID: evt.d.id
                    }, function (error) {
                        if (error) {
                            sendEmbed({
                                "title": "Barnacle, something went wrong mate! (Blame @GloomyJD for this...)",
                                "description": error
                            });
                        }
                    });
                    break;

                case 'test':
                    console.log(evt);
                    break;

                default:
                    send("Unknown command");
            }
        }
    }

    function hasRightArgs(cmd, args) {
        for (let command of commandsWithHelp) {
            if (command.name === cmd) {
                return args.length >= command.args && !command.disabled;
            }
        }
        return false;
    }

    function sendEmbed(embed) {
        embed.color = 13188608;
        bot.sendMessage({to: channelID, embed: embed}, function (error, response) {
            if (error) {
                console.log(error);
            }
        });
    }

    function send(message) {
        bot.sendMessage({to: channelID, message: message});
    }

    function sendTitle(pirate, ocean) {
        ocean = ocean || 'obsidian';
        let piratePage = 'http://' + ocean + '.puzzlepirates.com/yoweb/pirate.wm?target=' + pirate;
        request(piratePage, function (error, response, html) {
            if (!error && response.statusCode === 200) {
                let $ = cheerio.load(html);

                let title, pirateUrl;
                if ($("body center").text().length > 0) {
                    sendEmbed({
                        "title": "Unknown pirate",
                        "description": "Are ye sure this pirate exists mate?"
                    });
                } else {
                    title = $("body > table > tbody > tr > td:first-child").find("tbody tr").eq(1).find("tr[valign='middle'] td:nth-child(2) font").first().text();
                    if (title.length === 0) {
                        title = "This pirate be without a crew";
                    }
                    $("body > table > tbody > tr > td:nth-child(2)").find("img").each(function () {
                        let imageUrl = $(this).attr('src');
                        if (imageUrl.indexOf("http://") >= 0) {
                            pirateUrl = imageUrl
                        }
                    });
                    sendEmbed({
                        "title": capFirstLetter(pirate),
                        "description": title,
                        "url": piratePage,
                        "image": {
                            "url": (pirateUrl || "")
                        },
                        "fields": [{
                            "name": "Ocean",
                            "value": capFirstLetter(ocean),
                        }]
                    });
                }
            }
        });
    }

    function sendStanding(pirate, askedPuzzles, ocean) {
        ocean = ocean || 'obsidian';
        let piratePage = 'http://' + ocean + '.puzzlepirates.com/yoweb/pirate.wm?target=' + pirate;
        request(piratePage, function (error, response, html) {
            if (!error && response.statusCode === 200) {
                let $ = cheerio.load(html);

                if ($("body center").text().length > 0) {
                    sendEmbed({
                        "title": "Barnacle, something went wrong mate!",
                        "description": "Sorry mate, that won't work. Ye probably either misspelled one of the puzzles or the pirate yer lookin' for is a goner. Use `" + prefix + "standing help` for more information."
                    });
                } else {
                    let pirateUrl;
                    let embedFields = [{
                        "name": "Ocean",
                        "value": capFirstLetter(ocean),
                    }];

                    let realPuzzles = {};
                    if (askedPuzzles.length === 0) {
                        askedPuzzles = puzzles;
                    }
                    for (let puzzle of askedPuzzles) {
                        let puzzleIndex = puzzles.indexOf(puzzle);
                        if (puzzleIndex >= 0) {
                            realPuzzles[puzzle] = puzzleIndex;
                        } else {
                            embedFields.push({
                                "name": "Unknown puzzle",
                                "value": capFirstLetter(puzzle) + " is not a known puzzle to me mate"
                            });
                        }
                    }
                    $("body > table > tbody > tr > td:nth-child(3)").find("tbody tr tr[valign='middle'] td:nth-child(2)").each(function (index) {
                        for (let puzzle in realPuzzles) {
                            if (realPuzzles.hasOwnProperty(puzzle) && index === realPuzzles[puzzle]) {
                                let standing = "";
                                $(this).find("font").each(function () {
                                    standing += $(this).text();
                                });
                                embedFields.push({
                                    "name": capFirstLetter(puzzle),
                                    "value": standing
                                });
                            }
                        }
                    });
                    $("body > table > tbody > tr > td:nth-child(2)").find("img").each(function () {
                        let imageUrl = $(this).attr('src');
                        if (imageUrl.indexOf("http://") >= 0) {
                            pirateUrl = imageUrl
                        }
                    });
                    sendEmbed({
                        "title": capFirstLetter(pirate),
                        "url": piratePage,
                        "thumbnail": {
                            "url": (pirateUrl || "")
                        },
                        "fields": embedFields
                    });
                }
            } else {
                sendEmbed({
                    "title": "Barnacle, something went wrong mate!",
                    "description": "Does the ocean " + ocean + " even exist mate?"
                });
            }
        });
    }

    function sendStatus(ocean) {
        ocean = ocean || 'obsidian';

        request('http://www.puzzlepirates.com/status.xhtml', function (error, response, html) {
            if (!error && response.statusCode === 200) {
                let $ = cheerio.load(html);

                let oceans = ['emerald', 'cerulean', 'jade', 'ice', 'obsidian', 'opal', 'meridian'];
                let oceanIndex = oceans.indexOf(ocean.toLowerCase());
                if (oceanIndex >= 0) {
                    $(".dataTable tr").each(function (index, element) {
                        if (index === oceanIndex + 1) {
                            let status = $(this).find("td:first-child").text();
                            let userCount = $(this).find("td:last-child").text();
                            sendEmbed({
                                "title": capFirstLetter(ocean),
                                "fields": [
                                    {
                                        "name": "Status: " + status,
                                        "value": "Online pirates: " + userCount
                                    }
                                ]
                            });
                        }
                    });
                } else {
                    sendEmbed({
                        "title": "Barnacle, something went wrong mate!",
                        "description": "Are ye sure this ocean exists mate?"
                    });
                }
            }
        });
    }

    function sendPvp(pirate, number){
        let index = parseInt(number || 1) - 1;
        if (isNaN(index)) {
            return false;
        }
        request('http://obsidian.puzzlepirates.com/yoweb/pirate.wm?target=' + pirate, function (error, response, html) {
            if (!error && response.statusCode === 200) {
                let $ = cheerio.load(html);
                let intable = $("body table").eq(2);
                let crew = intable.find("tbody").eq(0).find("tr").eq(0).find("td").eq(1).find("a").eq(0).attr("href");
                let crewid = /\d+/g.exec(crew)[0];
                request('http://obsidian.puzzlepirates.com/yoweb/crew/battleinfo.wm?crewid=' + crewid, function (error, response, html) {
                    if (!error && response.statusCode === 200) {
                        let $ = cheerio.load(html);
                        let crew = $("body center > font").text();
                        let date = $("body table").eq(1).find("tr.small").eq(index).text().replace("At", "").trim().slice(0, -1);
                        let attacker = $("body table").eq(1).find("tr.small").eq(index).next().find("td:first-child").text().replace("from", "from ");
                        let defender = $("body table").eq(1).find("tr.small").eq(index).next().find("td:last-child").text().replace("from", "from ");
                        let result = $("body table").eq(1).find("tr.small").eq(index).next().next().text();
                        sendEmbed({
                            "title": crew,
                            "description": "Battle commenced at " + date,
                            "fields": [
                                {
                                    "name": "Attacker",
                                    "value": attacker
                                },
                                {
                                    "name": "Defender",
                                    "value": defender
                                },
                                {
                                    "name": "Result of battle",
                                    "value": result
                                }
                            ]
                        });
                    }
                });
            }
        });
        return true;
    }

    function sendStats(pirate, days) {
        days = parseInt(days || 1);
        if (isNaN(days)) {
            return false;
        }
        request('http://obsidian.puzzlepirates.com/yoweb/pirate.wm?target=' + pirate, function (error, response, html) {
            if (!error && response.statusCode === 200) {
                let $ = cheerio.load(html);
                let intable = $("body table").eq(2);
                let crew = intable.find("tbody").eq(0).find("tr").eq(0).find("td").eq(1).find("a").eq(0).attr("href");
                let crewid = /\d+/g.exec(crew)[0];
                request('http://obsidian.puzzlepirates.com/yoweb/crew/battleinfo.wm?crewid=' + crewid, function (error, response, html) {
                    if (!error && response.statusCode === 200) {
                        let $ = cheerio.load(html);
                        let crew = $("body center > font").text();
                        let row = $("body table").eq(0).find("tr").eq(days + 1);
                        let date = row.find("td:first-child").text();
                        let battles = row.find("td:nth-child(2)").text();
                        let wins = row.find("td:nth-child(3)").text();
                        let losses = row.find("td:nth-child(4)").text();
                        let pvpWins = parseInt(row.find("td:nth-child(5)").text());
                        let pvpLosses = parseInt(row.find("td:nth-child(6)").text());
                        let avgDuration = row.find("td:nth-child(7)").text().replace("\n", " ");
                        sendEmbed({
                            "title": crew,
                            "description": "Battle statistics for " + date,
                            "fields": [
                                {
                                    "name": "# of battles",
                                    "value": battles + " battles, (" + wins + " wins, " + losses + " losses)"
                                },
                                {
                                    "name": "# of PvP battles",
                                    "value": (pvpWins + pvpLosses) + " PvP-battles, (" + pvpWins + " wins, " + pvpLosses + " losses)"
                                },
                                {
                                    "name": "Average battle duration",
                                    "value": avgDuration
                                }
                            ]
                        });
                    }
                });
            }
        });
        return true;
    }

    function sendEvent(eventName, eventTime, eventDescription) {
        eventDescription = eventDescription || eventName;
        let eventDatetime;
        if (eventTime.length <= 5) {
            let timeString = moment().tz("America/Los_Angeles").format("YYYY-MM-DD ") + eventTime;
            eventDatetime = moment.tz(timeString, "America/Los_Angeles");

            if (eventDatetime.isBefore(moment())) {
                eventDatetime.add(1, 'days');
            }
        } else {
            eventDatetime = moment.tz(eventTime, "America/Los_Angeles");
        }
        if (eventDatetime === undefined || eventDatetime.format() === 'Invalid date') {
            sendEmbed({
                "title": "Barnacle, something went wrong mate!",
                "description": "Ye entered an invalid starting time for yer event, please check again"
            });
            return;
        }
        let discalTimeFormat = 'YYYY/MM/DD-HH:mm:ss';
        createEvent = [
            '&event summary ' + eventName,
            '&event description ' + eventDescription,
            '&event start ' + eventDatetime.format(discalTimeFormat),
            '&event end ' + eventDatetime.add(1, 'hours').format(discalTimeFormat),
            '&event review',
            '&event confirm'
        ];
        createEventIndex = 0;
        send('&event create');
    }
    
    function sendSMH(mapType) {
        request('https://yppedia.puzzlepirates.com/Chart#Obsidian_Ocean', {strictSSL: false}, function (error, response, html) {
            if (!error && response.statusCode === 200) {
                let $ = cheerio.load(html);
                let table = $("#bodyContent table.wikitable tbody");
                let formatToday = moment().format('MMM DD');
                let smhFound;
                let nextRound;
                mapType = mapType ? mapType.toLowerCase() : mapType;
                if (mapType && !smh.hasOwnProperty(mapType)) {
                    sendEmbed({
                        "title": "Unknown type",
                        "description": "Ahoy mate, I've me never heard of " + mapType + ". Tell TheRien what it is, would ye?"
                    });
                } else {
                    for (let i = 0; i < 28; i++) {
                        let day = moment().add(i, 'days');
                        let formatDay = day.format('MMM DD');
                        let cell = table.find("td:contains('" + formatDay + "')").first();
                        let currentSMH;
                        if (cell.text()) {
                            let tdIndex = cell.index() + 1 <= 5 ? 1 : 2;
                            currentSMH = cell.parent().next().find("td:nth-of-type(" + tdIndex + ")").text().trim();
                        }
                        if (!mapType && formatDay === formatToday) {
                            smhFound = currentSMH;
                            nextRound = day;
                            break;
                        } else if (currentSMH === smh[mapType] && moment().isSameOrBefore(day, 'day')) {
                            smhFound = currentSMH;
                            nextRound = day;
                            break;
                        }
                    }
                    if (!mapType && !smhFound) {
                        smhFound = table.find("tr:nth-child(4)").find("td:first-child").text().trim();
                    }
                    if (mapType) {
                        sendEmbed({
                            "title": "SMH Rotation",
                            "description": "Next time " + smhFound + " will drop: " + (moment().isSame(nextRound, 'day') ? 'today!' : nextRound.format("DD-MM-YYYY")),
                        });
                    } else {
                        sendEmbed({
                            "title": "SMH Rotation",
                            "description": "Current map drops: " + smhFound,
                        });
                    }
                }
            }
        });
    }
});

function capFirstLetter(string) {
    return string.toLowerCase().charAt(0).toUpperCase() + string.toLowerCase().substr(1).replace('-', ' ');
}